const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");
const auth = require('../auth.js');

// Route for creating a course
router.post("/", auth.verify,  (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin) {
	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send("User is notAdmin!");
	}
});


module.exports = router;

/*
	Mini-Activity:
		Limit the course creation to admin only. Refactor the course route/course controller.
*/